package com.example.jobapplicant

interface BasePresenter {
    fun onDestroy()
}