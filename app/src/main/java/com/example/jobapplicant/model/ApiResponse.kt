package com.example.jobapplicant.model


class ApiResponse(private val statusResponse: Long, private val data: String) {


    fun getStatus(): Long {
        return statusResponse
    }

    fun getData(): String {
        return data
    }

}