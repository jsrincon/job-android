package com.example.jobapplicant.model

import java.util.*

class Vacant private constructor(
    private val id: Long? = null,
    private val title: String? = null,
    private val position: String? = null,
    private val disponibility: String? = null,
    private val salary: Int? = null,
    private val priority: Boolean? = null,
    private val dateCreated: Date? = null,
    private val status: String? = null,
    private val description: String? = null,
    private val business: Business? = null
) {
    fun getId(): Long? {
        return id
    }

    fun getDisponibility(): String? {
        return disponibility
    }

    fun getSalary(): Int?{
        return  salary
    }

    fun getPriority():Boolean?{
        return priority
    }

    fun getDescription():String?{
        return description
    }

    fun getPosition(): String? {
        return position
    }


    fun getTitle(): String? {
        return title
    }


    data class Builder(

        private var id: Long? = null,
        private var title: String? = null,
        private var position: String? = null,
        private var disponibility: String? = null,
        private var salary: Int? = null,
        private var priority: Boolean? = null,
        private var dateCreated: Date? = null,
        private var status: String? = null,
        private var description: String? = null,
        private var business: Business? = null
    ) {
        fun id(id: Long) = apply { this.id = id }
        fun title(title: String) = apply { this.title = title }
        fun position(position: String) = apply { this.position = position }
        fun disponibility(disponibility: String) = apply { this.disponibility = disponibility }
        fun salary(salary: Int) = apply { this.salary = salary }
        fun priority(priority: Boolean) = apply { this.priority = priority }
        fun dateCreated(dateCreated: Date) = apply { this.dateCreated = dateCreated }
        fun status(status: String) = apply { this.status = status }
        fun description(description: String) = apply { this.description = description }
        fun business(business: Business) = apply { this.business = business }
        fun build() = Vacant(
            id,
            title,
            position,
            disponibility,
            salary,
            priority,
            dateCreated,
            status,
            description,
            business
        )

    }
}