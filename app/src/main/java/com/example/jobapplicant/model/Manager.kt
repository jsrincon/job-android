package com.example.jobapplicant.model

import java.util.*

class Manager private constructor(
    private val id: Long,
    private val name: String,
    private val lastName: String,
    private val phone: String,
    private val address: String,
    private val department: String,
    private val city: String,
    private val email: String,
    private val typeDocument: String,
    private val numberDocument: Long,
    private val birthdate: Date,
    private val username: String,
    private val password: String,
    private val businessList: List<Business>
) {

    data class Builder(
        private var id: Long,
        private var name: String,
        private var lastName: String,
        private var phone: String,
        private var address: String,
        private var department: String,
        private var city: String,
        private var email: String,
        private var typeDocument: String,
        private var numberDocument: Long,
        private var birthdate: Date,
        private var username: String,
        private var password: String,
        private var businessList: List<Business>
    ) {
        fun id(id: Long) = apply { this.id = id }
        fun name(name: String) = apply { this.name = name }
        fun lastName(lastName: String) = apply { this.lastName = lastName }
        fun phone(phone: String) = apply { this.phone = phone }
        fun address(address: String) = apply { this.address = address }
        fun department(department: String) = apply { this.department = department }
        fun city(city: String) = apply { this.city = city }
        fun email(email: String) = apply { this.email = email }
        fun typeDocument(typeDocument: String) = apply { this.typeDocument = typeDocument }
        fun numberDocument(numberDocument: Long) = apply { this.numberDocument = numberDocument }
        fun numberDocument(birthdate: Date) = apply { this.birthdate = birthdate }
        fun username(username: String) = apply { this.username = username }
        fun password(password: String) = apply { this.password = password }
        fun businessList(businessList:List<Business>) = apply { this.businessList = businessList }
    }
}