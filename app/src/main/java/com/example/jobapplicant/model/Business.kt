package com.example.jobapplicant.model

class Business private constructor(
    private val id: Long?,
    private val name: String,
    private val nit: Int,
    private val address: String,
    private val department: String,
    private val city: String,
    private val phone: Int,
    private val email: String,
    private val manager: Manager?
) {
    data class Builder(
        private var id: Long?,
        private var name: String,
        private var nit: Int,
        private var address: String,
        private var department: String,
        private var city: String,
        private var phone: Int,
        private var email: String,
        private var manager: Manager?
    ) {
        fun id(id: Long) = apply { this.id = id }
        fun name(name: String) = apply { this.name = name }
        fun nit(nit: Int) = apply { this.nit = nit }
        fun address(address: String) = apply { this.address = address }
        fun department(department: String) = apply { this.department = department }
        fun city(city: String) = apply { this.city = city }
        fun phone(phone: Int) = apply { this.phone = phone }
        fun email(email: String) = apply { this.email = email }
        fun manager(manager: Manager) = apply { this.manager = manager }
    }

    fun getId():Long?{
        return id
    }
    fun getName() : String? {
        return name
    }
    fun getNit() : Number? {
        return nit
    }
    fun getAddress() : String? {
        return address
    }
    fun getDepartment() : String? {
        return department
    }
    fun getCity() : String? {
        return city
    }
    fun getPhone() : Number? {
        return phone
    }
    fun getEmail() : String? {
        return email
    }
}