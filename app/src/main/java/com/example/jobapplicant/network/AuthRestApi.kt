package com.example.jobapplicant.network

import com.example.jobapplicant.model.ApiResponse
import com.example.jobapplicant.model.LoginRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST


interface AuthRestApi {

    @Headers("Content-Type: application/json")
    @POST("auth/login")
    fun authentication(@Body login: LoginRequest): Call<ApiResponse>

}