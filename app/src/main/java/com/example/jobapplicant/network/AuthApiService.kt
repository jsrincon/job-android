package com.example.jobapplicant.network

import android.util.Log
import com.example.jobapplicant.model.ApiResponse
import com.example.jobapplicant.model.LoginRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthApiService {
    fun authentication(login: LoginRequest, onResult: (ApiResponse?) -> Unit) {

        val retrofit = ServiceBuilder.buildService(AuthRestApi::class.java)
        retrofit.authentication(login).enqueue(
            object : Callback<ApiResponse> {
                override fun onFailure(call: Call<ApiResponse>, t: Throwable) {
                    Log.e("LoginInteractor: ", t.toString());
                }
                override fun onResponse( call: Call<ApiResponse>, response: Response<ApiResponse>) {
                    val addedUser = response.body()
                    onResult(addedUser)
                }
        })
    }
}