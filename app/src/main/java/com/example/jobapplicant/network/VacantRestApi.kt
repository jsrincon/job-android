package com.example.jobapplicant.network

import com.example.jobapplicant.model.Vacant
import retrofit2.Call
import retrofit2.http.*


interface VacantRestApi {

    @Headers("Content-Type: application/json")
    @GET("vacant/{id}/manager")
    fun getVacant(@Path("id") id: Long): Call<ArrayList<Vacant>>

    @Headers("Content-Type: application/json")
    @GET("vacant/{id}")
    fun getVacantById(@Path("id") id: Long): Call<Vacant>

    @Headers("Content-Type: application/json")
    @POST("vacant/create-vacant")
    fun saveVacant(@Body vacant: Vacant): Call<Vacant>

}