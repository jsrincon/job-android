package com.example.jobapplicant.network

import android.util.Log
import com.example.jobapplicant.model.Vacant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VacantApiService {
    fun getVacanciesByManager(id: Long, onResult: (ArrayList<Vacant>?) -> Unit) {

        val retrofit = ServiceBuilder.buildService(VacantRestApi::class.java)
        retrofit.getVacant(id).enqueue(
            object : Callback<ArrayList<Vacant>> {
                override fun onFailure(call: Call<ArrayList<Vacant>>, t: Throwable) {
                    Log.e("VacantInteractor: ", t.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<Vacant>>,
                    response: Response<ArrayList<Vacant>>
                ) {
                    val listResponse = response.body()
                    onResult(listResponse)
                }
            })
    }

    fun getVacanciesByIdr(id: Long, onResult: (Vacant?) -> Unit) {

        val retrofit = ServiceBuilder.buildService(VacantRestApi::class.java)
        retrofit.getVacantById(id).enqueue(
            object : Callback<Vacant> {
                override fun onFailure(call: Call<Vacant>, t: Throwable) {
                    Log.e("VacantInteractor: ", t.toString())
                }

                override fun onResponse(
                    call: Call<Vacant>,
                    response: Response<Vacant>
                ) {
                    val data = response.body()
                    onResult(data)
                }
            })
    }

    fun saveVacant(vacant: Vacant, onResult: (Vacant) -> Unit) {

        val retrofit = ServiceBuilder.buildService(VacantRestApi::class.java)
        retrofit.saveVacant(vacant).enqueue(
            object : Callback<Vacant> {
                override fun onFailure(call: Call<Vacant>, t: Throwable) {
                    Log.e("VacantInteractor: ", t.toString())
                }

                override fun onResponse(
                    call: Call<Vacant>,
                    response: Response<Vacant>
                ) {
                    response.body()?.let { onResult(it) }
                }
            })
    }
}