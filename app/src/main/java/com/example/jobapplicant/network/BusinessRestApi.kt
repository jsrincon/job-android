package com.example.jobapplicant.network

import com.example.jobapplicant.model.Business
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface BusinessRestApi {

    @Headers("Content-Type: application/json")
    @GET("business/{id}/manager")
    fun getBusiness(@Path("id") id : Long): Call<ArrayList<Business>>

    @Headers("Content-Type: application/json")
    @GET("business/{id}")
    fun getBusinessById(@Path("id") id : Long): Call<Business>
}