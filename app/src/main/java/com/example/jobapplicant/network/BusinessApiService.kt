package com.example.jobapplicant.network

import android.util.Log
import com.example.jobapplicant.model.Business
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BusinessApiService {
    fun getBusinessByManager(id: Long, onResult: (ArrayList<Business>?) -> Unit) {

        val retrofit = ServiceBuilder.buildService(BusinessRestApi::class.java)
        retrofit.getBusiness(id).enqueue(
            object : Callback<ArrayList<Business>> {
                override fun onFailure(call: Call<ArrayList<Business>>, t: Throwable) {
                    Log.e("BusinessInteractor : ", t.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<Business>>,
                    response: Response<ArrayList<Business>>
                ) {
                    val listResponse = response.body()
                    onResult(listResponse)
                }
            }
        )
    }

    fun getBusinessById(id: Long, onResult: (Business?) -> Unit) {

        val retrofit = ServiceBuilder.buildService(BusinessRestApi::class.java)
        retrofit.getBusinessById(id).enqueue(
            object : Callback<Business> {
                override fun onFailure(call: Call<Business>, t: Throwable) {
                    Log.e("BusinessInteractor : ", t.toString())
                }

                override fun onResponse(
                    call: Call<Business>,
                    response: Response<Business>
                ) {
                    val listResponse = response.body()
                    onResult(listResponse)
                }
            }
        )
    }
}