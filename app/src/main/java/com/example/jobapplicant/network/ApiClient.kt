package com.example.jobapplicant.network

import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ServiceBuilder {

    var client = OkHttpClient.Builder().addInterceptor { chain ->
        val newRequest: Request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzb3V0aCIsIlJPTEVTIjpbIlJPTEVfQVBQTElDQU5UIiwiUk9MRV9VU0VSIl0sImV4cCI6MTYyMjA3OTQ2OCwiaWF0IjoxNjIyMDYxNDY4fQ.VKGfjMTXUDQRZoPsTaHZnu9sT23g3d5FHJalzjl5vCT-xnqTwIuau0p6JYoMUPSx-PUZUghLLMSp6UXQSGh2KA")
            .build()
        chain.proceed(newRequest)
    }.build()

    private val retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl("http://nameless-ridge-68194.herokuapp.com/api/job/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun <T> buildService(service: Class<T>): T {
        return retrofit.create(service)
    }
}