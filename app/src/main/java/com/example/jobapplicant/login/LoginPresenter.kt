package com.example.jobapplicant.login

class LoginPresenter(
    private var loginView: LoginContract.LoginView?,
    private val loginInteractor: LoginInteractor
) : LoginContract.Presenter, LoginContract.Model.OnLoginFinishedListener {

    override fun validateCredentials(username: String, password: String) {
        loginView?.showProgress()
        loginInteractor.login(username, password, this)
    }

    override fun onDestroy() {
        loginView = null
    }

    override fun onUsernameError() {
        loginView?.apply {
            setUsernameError()
            hideProgress()
        }
    }

    override fun onPasswordError() {
        loginView?.apply {
            setPasswordError()
            hideProgress()
        }
    }

    override fun onSuccess() {
        loginView?.navigateToHome()
        loginView?.hideProgress()
    }

    override fun onError() {
        loginView?.hideProgress()
        loginView?.setToastError()
    }
}