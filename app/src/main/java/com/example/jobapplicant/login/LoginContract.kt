package com.example.jobapplicant.login

import com.example.jobapplicant.BasePresenter
import com.example.jobapplicant.BaseView

interface LoginContract {
    interface Model {
        interface OnLoginFinishedListener {
            fun onUsernameError()
            fun onPasswordError()
            fun onSuccess()
            fun onError()
        }

        fun login(username: String, password: String, listener: OnLoginFinishedListener)
    }

    interface LoginView:BaseView<Presenter> {
        fun setUsernameError()
        fun setPasswordError()
        fun navigateToHome()
        fun setToastError()
    }

    interface Presenter:BasePresenter {
        fun validateCredentials(username: String, password: String)
    }
}