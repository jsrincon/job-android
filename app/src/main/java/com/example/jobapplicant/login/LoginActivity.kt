package com.example.jobapplicant.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.jobapplicant.R
import com.example.jobapplicant.databinding.ActivityLoginBinding
import com.example.jobapplicant.home.HomeManager


class LoginActivity : AppCompatActivity(), LoginContract.LoginView {
    private var mPresenter: LoginContract.Presenter? = null
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachPresenter(LoginPresenter(this, LoginInteractor()))
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.button.setOnClickListener { validateCredentials() }
    }

    private fun validateCredentials() {
        mPresenter?.validateCredentials(
            binding.username.text.toString(),
            binding.password.text.toString()
        )
    }

    override fun onDestroy() {
        detachPresenter()
        super.onDestroy()
    }

    override fun showProgress() {
        binding.progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progress.visibility = View.GONE
    }

    override fun setUsernameError() {
        binding.username.error = getString(R.string.username_error)
    }

    override fun setPasswordError() {
        binding.password.error = getString(R.string.password_error)
    }

    override fun navigateToHome() {
        startActivity(Intent(this, HomeManager::class.java))
    }

    override fun setToastError() {
        val duration = Toast.LENGTH_SHORT
        Toast.makeText(applicationContext, getString(R.string.credentials), duration).show()
    }

    override fun attachPresenter(presenter: LoginContract.Presenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }
}