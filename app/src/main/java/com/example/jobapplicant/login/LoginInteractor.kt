package com.example.jobapplicant.login

import com.example.jobapplicant.model.LoginRequest
import com.example.jobapplicant.network.AuthApiService

class LoginInteractor() : LoginContract.Model {
    private val apiService = AuthApiService()

    override fun login(
        username: String,
        password: String,
        listener: LoginContract.Model.OnLoginFinishedListener
    ) {
        if (username.isEmpty()) listener.onUsernameError()
        if (password.isEmpty()) listener.onPasswordError()

        val loginRequest = LoginRequest(username = username, password = password)

        apiService.authentication(loginRequest) {
            if (it?.getStatus() == 200L) {
                listener.onSuccess()
            } else {
                listener.onError()
            }
        }
    }
}