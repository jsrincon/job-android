package com.example.jobapplicant.home.vacant.form

import com.example.jobapplicant.BasePresenter
import com.example.jobapplicant.BaseView
import com.example.jobapplicant.model.Vacant

interface FormVacantContract {
    interface FormPresenter : BasePresenter {
        fun saveVacant(
            title: String,
            description: String,
            disponibility: String,
            position: String,
            priority: String,
            salary: String
        )
    }

    interface VacantFormView : BaseView<FormPresenter> {
        fun save()
        fun setToastError()
    }

    interface Model {
        interface OnFormVacantFinishedListener {
            fun onSaveError()
            fun onSuccess(vacant: Vacant)
            fun onError()
        }
    }
}