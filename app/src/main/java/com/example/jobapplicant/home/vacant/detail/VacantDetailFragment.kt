package com.example.jobapplicant.home.business.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jobapplicant.BaseFragment
import com.example.jobapplicant.databinding.FragmentVacantDetailBinding
import com.example.jobapplicant.home.vacant.VacantContract
import com.example.jobapplicant.home.vacant.VacantInteractor
import com.example.jobapplicant.home.vacant.detail.VacantDetailPresenter
import com.example.jobapplicant.model.Vacant

class VacantDetailFragment : BaseFragment(), VacantContract.VacantDetailView {

    var id: Long = 0
    private var mPresenter: VacantContract.VacantDetailPresenter? = null
    private var _binding: FragmentVacantDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVacantDetailBinding.inflate(inflater, container, false)

        val root: View = binding.root
        attachPresenter(VacantDetailPresenter(this, VacantInteractor()))

        arguments?.let {
            id = it.getLong("id")
        }

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter?.getVacantById(id)
    }

    override fun refreshFragment(vacant: Vacant) {
        binding.valueTitle.text = vacant.getTitle()
        binding.valuePosition.text = vacant.getPosition()
        binding.valueDisponibility.text = vacant.getDisponibility()
        binding.valueSalary.text = vacant.getSalary().toString()
        binding.valueDescription.text = vacant.getDescription()
        binding.valuePriority.text = vacant.getPriority().toString()
    }

    override fun setToastError() {
        TODO("Not yet implemented")
    }

    override fun attachPresenter(presenter: VacantContract.VacantDetailPresenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }


    override fun showProgress() {
        binding.vacantProgress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.vacantProgress.visibility = View.GONE
    }

}