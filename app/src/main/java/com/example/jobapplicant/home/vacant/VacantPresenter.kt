package com.example.jobapplicant.home.vacant

import com.example.jobapplicant.model.Vacant

class VacantPresenter(
    private var vacantView: VacantContract.VacantView?,
    private val vacantInteractor: VacantInteractor
) : VacantContract.Presenter, VacantContract.Model.OnVacantFinishedListener {
    override fun onChangeError() {
        vacantView?.hideProgress()
        vacantView?.setToastError()
    }

    override fun onSaveError() {
        vacantView?.hideProgress()
        vacantView?.setToastError()
    }

    override fun onSuccess(list: ArrayList<Vacant>) {
        vacantView?.refreshFragment(list)
        vacantView?.hideProgress()
    }

    override fun onError() {
        vacantView?.hideProgress()
        vacantView?.setToastError()
    }

    override fun onDestroy() {
        vacantView = null
    }

    override fun getVacant(){
        vacantView?.showProgress()
        vacantInteractor.getVacant(this)
    }

//    override fun addVacant() {
//        TODO("Not yet implemented")
//    }

    override fun show(vacant: Vacant) {
        vacantView?.navigateToDetailVacant()
    }

//    override fun changeStatus(vacant: Vacant) {
//        TODO("Not yet implemented")
//    }
}