package com.example.jobapplicant.home.vacant.detail

import com.example.jobapplicant.home.vacant.VacantContract
import com.example.jobapplicant.home.vacant.VacantInteractor
import com.example.jobapplicant.model.Vacant

class VacantDetailPresenter(
    private var vacantView: VacantContract.VacantDetailView?,
    private val vacantInteractor: VacantInteractor
) : VacantContract.VacantDetailPresenter, VacantContract.Model.OnVacantDetailFinishedListener {


    override fun getVacantById(id: Long) {
        vacantView?.showProgress()
        vacantInteractor.getVacantById(id, this)
    }

    override fun onDestroy() {
        vacantView = null
    }

    override fun onSuccess(vacant: Vacant) {
        vacantView?.refreshFragment(vacant)
        vacantView?.hideProgress()
    }

    override fun onError() {
        vacantView?.hideProgress()
        vacantView?.setToastError()
    }

}