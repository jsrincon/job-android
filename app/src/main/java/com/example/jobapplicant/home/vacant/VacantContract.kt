package com.example.jobapplicant.home.vacant

import com.example.jobapplicant.BasePresenter
import com.example.jobapplicant.BaseView
import com.example.jobapplicant.home.vacant.form.FormVacantContract
import com.example.jobapplicant.model.Business
import com.example.jobapplicant.model.Vacant

interface VacantContract {
    interface Model {
        interface OnVacantFinishedListener {
            fun onChangeError()
            fun onSaveError()
            fun onSuccess(list: ArrayList<Vacant>)
            fun onError()
        }

        interface OnVacantDetailFinishedListener {
            fun onSuccess(vacant: Vacant)
            fun onError()
        }

        fun getVacant(listener: OnVacantFinishedListener)
        fun getVacantById(id: Long, listener: OnVacantDetailFinishedListener)
        fun addVacant(
            vacant: Vacant,
            listener: FormVacantContract.Model.OnFormVacantFinishedListener
        )
    }

    interface VacantView : BaseView<Presenter> {
        fun navigateToDetailVacant()
        fun refreshFragment(list: ArrayList<Vacant>)
        fun setToastError()
    }

    interface Presenter : BasePresenter {
        fun getVacant()
        fun show(vacant: Vacant)
    }

    interface VacantDetailView : BaseView<VacantDetailPresenter> {
        fun refreshFragment(vacant: Vacant)
        fun setToastError()
    }


    interface VacantDetailPresenter : BasePresenter {
        fun getVacantById(id: Long)
    }

}