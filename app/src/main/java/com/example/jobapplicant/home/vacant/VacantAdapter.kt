package com.example.jobapplicant.home.vacant

import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.jobapplicant.R
import com.example.jobapplicant.model.Vacant

class VacantAdapter(val data: ArrayList<Vacant>) :
    RecyclerView.Adapter<VacantAdapter.VacantViewHolder>() {

    class VacantViewHolder(val layout: View) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): VacantViewHolder {
        val layout =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_list_item, parent, false)
        return VacantViewHolder(layout)
    }

    override fun onBindViewHolder(holder: VacantViewHolder, position: Int) {
        val title = holder.layout.findViewById<TextView>(R.id.txtTitle)
        val description = holder.layout.findViewById<TextView>(R.id.txtDescription)
        title.text = data[position].getTitle()
        description.text = "Id:" + data[position].getId().toString()

        val bundle = Bundle()
        data[position].getId()?.let { bundle.putLong("id", it) }

        holder.layout.findViewById<CardView>(R.id.card).setOnClickListener {view ->
            view.findNavController().navigate(R.id.action_nav_vacant_to_vacantDetailFragment, bundle)
        }

    }

    override fun getItemCount() = data.size

}