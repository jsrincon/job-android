package com.example.jobapplicant.home.business.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jobapplicant.BaseFragment
import com.example.jobapplicant.databinding.FragmentBusinessBinding
import com.example.jobapplicant.databinding.FragmentBusinessDetailBinding
import com.example.jobapplicant.home.business.BusinessContract
import com.example.jobapplicant.home.business.BusinessInteractor
import com.example.jobapplicant.home.business.BusinessPresenter
import com.example.jobapplicant.home.vacant.VacantContract
import com.example.jobapplicant.model.Business

class BusinessDetailFragment : BaseFragment(), BusinessContract.BusinessDetailView {

    var id: Long = 0
    private var mPresenter: BusinessContract.DetailPresenter? = null
    private var _binding: FragmentBusinessDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBusinessDetailBinding.inflate(inflater, container, false)

        val root: View = binding.root
        attachPresenter(DetailPresenter(this, BusinessInteractor()))

        arguments?.let {
            id = it.getLong("id")
        }

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter?.getBusinessById(id)
    }

    override fun refreshFragment(business: Business) {

        binding.valueName.text = business.getName()
        binding.valueNit.text = business.getNit().toString()
        binding.valueAddress.text = business.getAddress()
        binding.valueCity.text = business.getCity()
        binding.valueDepartment.text = business.getDepartment()
        binding.valuePhone.text = business.getPhone().toString()
        binding.valueEmail.text = business.getEmail()
    }

    override fun setToastError() {
        TODO("Not yet implemented")
    }

    override fun attachPresenter(presenter: BusinessContract.DetailPresenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }


    override fun showProgress() {
        binding.businessProgress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.businessProgress.visibility = View.GONE
    }

}