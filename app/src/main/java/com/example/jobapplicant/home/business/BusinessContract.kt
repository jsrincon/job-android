package com.example.jobapplicant.home.business

import com.example.jobapplicant.BasePresenter
import com.example.jobapplicant.BaseView
import com.example.jobapplicant.model.Business

interface BusinessContract {
    interface Model {
        interface OnBusinessFinishedListener {
            fun onSuccess(list: ArrayList<Business>)
            fun onError()
        }

        interface OnBusinessDetailFinishedListener {
            fun onSuccess(business: Business)
            fun onError()
        }

        fun getBusiness(listener: OnBusinessFinishedListener)
        fun getBusinessById(id: Long, listener: OnBusinessDetailFinishedListener)
    }

    interface BusinessView : BaseView<Presenter> {
        fun refreshFragment(list: ArrayList<Business>)
        fun navigateToDetailBusiness()
        fun setToastError()
    }

    interface BusinessDetailView : BaseView<DetailPresenter> {
        fun refreshFragment(business: Business)
        fun setToastError()
    }

    interface Presenter : BasePresenter {
        fun getBusiness()
    }

    interface DetailPresenter : BasePresenter {
        fun getBusinessById(id:Long)
    }

}