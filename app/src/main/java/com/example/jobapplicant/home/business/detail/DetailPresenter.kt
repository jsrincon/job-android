package com.example.jobapplicant.home.business.detail

import com.example.jobapplicant.home.business.BusinessContract
import com.example.jobapplicant.home.business.BusinessInteractor
import com.example.jobapplicant.model.Business

class DetailPresenter(
    private var businessView: BusinessContract.BusinessDetailView?,
    private val businessInteractor: BusinessInteractor
) : BusinessContract.DetailPresenter, BusinessContract.Model.OnBusinessDetailFinishedListener {


    override fun getBusinessById(id:Long) {
        businessView?.showProgress()
        businessInteractor.getBusinessById(id,this)    }

    override fun onDestroy() {
        businessView = null
    }

    override fun onSuccess(business: Business) {
        businessView?.refreshFragment(business)
        businessView?.hideProgress()
    }

    override fun onError() {
        businessView?.hideProgress()
        businessView?.setToastError()
    }

}