package com.example.jobapplicant.home.business

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.support.v4.app.Fragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.widget.Adapter
import android.widget.Toast
import com.example.jobapplicant.BaseFragment
import com.example.jobapplicant.R
import com.example.jobapplicant.databinding.FragmentBusinessBinding
import com.example.jobapplicant.model.Business

class BusinessFragment : BaseFragment(), BusinessContract.BusinessView {
    private var mPresenter: BusinessContract.Presenter? = null
    private var _binding: FragmentBusinessBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBusinessBinding.inflate(inflater, container, false)

        val root: View = binding.root
        attachPresenter(BusinessPresenter(this, BusinessInteractor()))
        binding.fabBusiness.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter?.getBusiness()
    }

    override fun refreshFragment(list: ArrayList<Business>) {
        viewAdapter = BusinessAdapter(list)
        recyclerView = binding.businessList
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = viewAdapter
    }

    override fun navigateToDetailBusiness() {
        TODO("Not yet implemented")
    }

    override fun setToastError() {
        val duration = Toast.LENGTH_SHORT
        Toast.makeText(context, getString(R.string.credentials), duration).show()
    }

    override fun attachPresenter(presenter: BusinessContract.Presenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }

    override fun showProgress() {
        binding.businessProgress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.businessProgress.visibility = View.GONE
    }


}