package com.example.jobapplicant.home.vacant

import com.example.jobapplicant.home.vacant.form.FormVacantContract
import com.example.jobapplicant.model.Vacant
import com.example.jobapplicant.network.VacantApiService

class VacantInteractor : VacantContract.Model {
    private val apiService = VacantApiService()

    override fun getVacant(listener: VacantContract.Model.OnVacantFinishedListener) {
        apiService.getVacanciesByManager(27L) {
            if (it?.isNotEmpty() == true) {
                listener.onSuccess(it)
            } else {
                listener.onError()
            }
        }
    }

    override fun getVacantById(
        id: Long,
        listener: VacantContract.Model.OnVacantDetailFinishedListener
    ) {
        apiService.getVacanciesByIdr(id) {
            if (it != null) {
                listener.onSuccess(it)
            } else {
                listener.onError()
            }
        }
    }

    override fun addVacant(
        vacant: Vacant,
        listener: FormVacantContract.Model.OnFormVacantFinishedListener
    ) {
        apiService.saveVacant(vacant) {
            listener.onSuccess(it)
        }
    }

}