package com.example.jobapplicant.home.business

import com.example.jobapplicant.network.BusinessApiService

class BusinessInteractor : BusinessContract.Model {
    private val apiService = BusinessApiService()

    override fun getBusiness(listener: BusinessContract.Model.OnBusinessFinishedListener) {
        apiService.getBusinessByManager(27L) {
            if (it?.isNotEmpty() == true) {
                listener.onSuccess(it)
            } else {
                listener.onError()
            }
        }
    }

    override fun getBusinessById(
        id: Long,
        listener: BusinessContract.Model.OnBusinessDetailFinishedListener
    ) {
        apiService.getBusinessById(id) {
            if (it != null) {
                listener.onSuccess(it)
            } else {
                listener.onError()
            }
        }
    }


}