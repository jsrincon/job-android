package com.example.jobapplicant.home.business

import com.example.jobapplicant.model.Business

class BusinessPresenter(
    private var businessView: BusinessContract.BusinessView?,
    private val businessInteractor: BusinessInteractor
) : BusinessContract.Presenter, BusinessContract.Model.OnBusinessFinishedListener {
    override fun getBusiness() {
        businessView?.showProgress()
        businessInteractor.getBusiness(this)
    }

    override fun onDestroy() {
        businessView = null
    }

    override fun onSuccess(list: ArrayList<Business>) {
        businessView?.refreshFragment(list)
        businessView?.hideProgress()
    }

    override fun onError() {
        businessView?.hideProgress()
        businessView?.setToastError()
    }

}