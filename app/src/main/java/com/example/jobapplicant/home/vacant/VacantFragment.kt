package com.example.jobapplicant.home.vacant

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.jobapplicant.BaseFragment
import com.example.jobapplicant.R
import com.example.jobapplicant.databinding.FragmentVacantBinding
import com.example.jobapplicant.home.vacant.form.FormVacant
import com.example.jobapplicant.model.Vacant

class VacantFragment : BaseFragment(),
    VacantContract.VacantView {

    private var mPresenter: VacantContract.Presenter? = null
    private var _binding: FragmentVacantBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentVacantBinding.inflate(inflater, container, false)

        val root: View = binding.root

        attachPresenter(VacantPresenter(this, VacantInteractor()))
        binding.fab.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_nav_vacant_to_formVacant)
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter?.getVacant()
    }

    override fun showProgress() {
        binding.progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progress.visibility = View.GONE
    }

    override fun navigateToDetailVacant() {
        TODO("Not yet implemented")
    }

    override fun refreshFragment(list: ArrayList<Vacant>) {
        viewAdapter = VacantAdapter(list)
        recyclerView = binding.vacantList
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = viewAdapter
    }

    override fun setToastError() {
        val duration = Toast.LENGTH_SHORT
        Toast.makeText(context, getString(R.string.credentials), duration).show()
    }

    override fun attachPresenter(presenter: VacantContract.Presenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }


}