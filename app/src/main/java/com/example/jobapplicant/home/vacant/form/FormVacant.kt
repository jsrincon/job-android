package com.example.jobapplicant.home.vacant.form

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.jobapplicant.R
import com.example.jobapplicant.databinding.ActivityFormVacantBinding
import com.example.jobapplicant.databinding.ActivityLoginBinding
import com.example.jobapplicant.home.vacant.VacantContract
import com.example.jobapplicant.home.vacant.VacantInteractor
import com.example.jobapplicant.home.vacant.VacantPresenter
import com.example.jobapplicant.login.LoginContract
import com.example.jobapplicant.login.LoginInteractor
import com.example.jobapplicant.login.LoginPresenter
import com.example.jobapplicant.model.Vacant

class FormVacant : AppCompatActivity() , FormVacantContract.VacantFormView{
    private var mPresenter: FormVacantContract.FormPresenter? = null
    private lateinit var binding: ActivityFormVacantBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachPresenter(FormVacantPresenter(this, VacantInteractor()))
        binding = ActivityFormVacantBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.btRegister.setOnClickListener { save() }
    }

    override fun save() {
        val title: String = binding.formVacantTitle.text.toString()
        val description: String = binding.formVacantDescription.text.toString()
        val disponibility: String = binding.formVacantDisponibility.text.toString()
        val position: String = binding.formVacantPosition.text.toString()
        val priority: String = binding.formVacantPriority.text.toString()
        val salary: String = binding.formVacantSalary.text.toString()

        mPresenter?.saveVacant(title,description,disponibility,position,priority,salary)
    }

    override fun setToastError() {
        val duration = Toast.LENGTH_SHORT
        Toast.makeText(applicationContext, getString(R.string.unknown_error), duration).show()
    }


    override fun attachPresenter(presenter: FormVacantContract.FormPresenter) {
        mPresenter = presenter
    }

    override fun detachPresenter() {
        mPresenter?.onDestroy()
        mPresenter = null
    }

    override fun showProgress() {
        binding.progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progress.visibility = View.GONE
    }
}