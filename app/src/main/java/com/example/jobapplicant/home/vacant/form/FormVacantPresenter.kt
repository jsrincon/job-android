package com.example.jobapplicant.home.vacant.form

import com.example.jobapplicant.home.vacant.VacantInteractor
import com.example.jobapplicant.model.Vacant

class FormVacantPresenter(
    private var formVacantView: FormVacantContract.VacantFormView?,
    private val vacantInteractor: VacantInteractor
) : FormVacantContract.FormPresenter, FormVacantContract.Model.OnFormVacantFinishedListener {

    override fun saveVacant(
        title: String,
        description: String,
        disponibility: String,
        position: String,
        priority: String,
        salary: String
    ) {
        val vacant: Vacant = Vacant.Builder()
            .title(title)
            .description(description)
            .disponibility(disponibility)
            .position(position)
            .priority(true)
            .salary(salary.toInt())
            .status("Status")
            .build()

        vacantInteractor.addVacant(vacant, this)
    }


    override fun onDestroy() {
        formVacantView = null
    }

    override fun onSaveError() {
        formVacantView?.hideProgress()
        formVacantView?.setToastError()
    }

    override fun onSuccess(vacant: Vacant) {
        formVacantView?.hideProgress()
    }

    override fun onError() {
        formVacantView?.hideProgress()
        formVacantView?.setToastError()
    }

}