package com.example.jobapplicant.home.business

import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import com.example.jobapplicant.R
import com.example.jobapplicant.model.Business


class BusinessAdapter(val data: ArrayList<Business>) :
    RecyclerView.Adapter<BusinessAdapter.BusinessViewHolder>() {
    class BusinessViewHolder(val layout: View) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BusinessViewHolder {
        val layout = LayoutInflater.from(p0.context).inflate(R.layout.layout_list_item, p0, false)
        return BusinessViewHolder(layout)
    }

    override fun onBindViewHolder(p0: BusinessViewHolder, p1: Int) {
        val title = p0.layout.findViewById<TextView>(R.id.txtTitle)
        val description = p0.layout.findViewById<TextView>(R.id.txtDescription)
        title.text = data[p1].getName()
        description.text = "Nit:" + data[p1].getNit().toString()

        val bundle = Bundle()
        data[p1].getId()?.let { bundle.putLong("id", it) }

        p0.layout.findViewById<CardView>(R.id.card).setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_nav_business_to_businessDetail, bundle)
        }
    }

    override fun getItemCount() = data.size
}